﻿using System;
using System.Collections.Generic;

namespace Generics.Robots
{
	public interface IRobotAI<out TCommand>
	{
		TCommand GetCommand();
	}

	public interface IDevice<in TCommand>
	{
		string ExecuteCommand(TCommand command);
	}

	public class ShooterAI : IRobotAI<ShooterCommand>
	{
		private int counter = 1;

		public ShooterCommand GetCommand() => ShooterCommand.ForCounter(counter++);
	}

    public class BuilderAI : IRobotAI<BuilderCommand>
    {
	    private int counter = 1;

        public BuilderCommand GetCommand() => BuilderCommand.ForCounter(counter++);
    }

    public class Mover : IDevice<IMoveCommand>
    {
        public string ExecuteCommand(IMoveCommand command)
        {
            if (command == null)
                throw new ArgumentException();
            return $"MOV {command.Destination.X}, {command.Destination.Y}";
		}
    }
	
    public class Robot<TCommand>
    {
	    private readonly IRobotAI<TCommand> ai;
	    private readonly IDevice<TCommand> device;

        public Robot(IRobotAI<TCommand> ai, IDevice<TCommand> executor)
        {
            this.ai = ai;
            this.device = executor;
        }

        public IEnumerable<string> Start(int steps)
        {
	        for (var i = 0; i < steps; i++)
	        {
		        var command = ai.GetCommand();
		        if (command == null)
			        break;
		        yield return device.ExecuteCommand(command);
	        }
        }
    }

    public class Robot
    {
	    public static Robot<TCommand> Create<TCommand>(IRobotAI<TCommand> ai, IDevice<TCommand> executor) =>
		    new Robot<TCommand>(ai, executor);
    }
}
